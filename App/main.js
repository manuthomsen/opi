
// Test to see if the browser supports the HTML template element by checking
// for the presence of the template element's content attribute.
if ('content' in document.createElement('template')) {

    var template = document.querySelector("#post-template");
    var tempClone = document.importNode(template.content,true);
    var wrapper = document.getElementById("post-wrapper");
    
    var postContainer = tempClone.querySelector(".post-container");
    var postName = tempClone.querySelector(".post-name");

    var postImage = tempClone.querySelector("img");
    postName.textContent = "WOW";
    
    

    wrapper.appendChild(tempClone);
    

} else {
  // Find another way to add the rows to the table because 
  // the HTML template element is not supported.
}